const infopack = require('infopack');
const folderToInfopack = require('infopack/dist/lib/generators/folder-to-infopack');
const mdToHtml = require('infopack-gen-md-to-html')

/**
 * Title: The visual name of the infopack (used in registry)
 * See https://schemas.infopack.io/infopack-index.2.schema.json for details
 */
const title = '<PACKAGE_TITLE>'
/**
 * Namespace: The namespace which this infopack will be published under
 * See https://schemas.infopack.io/infopack-index.2.schema.json for details
 */
const namespace = '<PACKAGE_NAMESPACE>'

let pipelineSteps = [
    // change/edit as you like
    folderToInfopack.step(),
    mdToHtml.step()
];

let pipeline = new infopack.default(pipelineSteps, { title, namespace });

pipeline.run();
